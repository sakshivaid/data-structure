/* Doubly linked list */

#include <iostream>
using namespace std;

class Node {
private:
	int data;
	Node *next, *prev;

public:
	Node():data(0),next(NULL), prev(NULL){}
	Node(int d):data(d),next(NULL), prev(NULL){}

	int get_data() {
		return data;
	}
	Node* get_next_ptr() {
		return next;
	}

	Node* get_prev_ptr(){
        return prev;
	}

	void set_node(int d) {
		data = d;

	}
	void set_next(Node *n) {
			next = n;
	}

	void set_prev(Node *n){
            prev = n;

	}
};

class link_list {
private:
	Node *head;
	int list_size;

public:

	link_list():head(NULL), list_size(0){}

	void insert_beg() {
		int d;
		cout<<"Enter data"<<endl;
		cin>>d;
		Node *n = new Node(d);

		if(head == NULL){
			head = n;
			list_size++;
		}

		else {

			n->set_next(head);
			head->set_prev(n);
			head = n;
			list_size++;
		}
	}

	void insert_end() {


		if(head == NULL) {
			insert_beg();
		}

		else {

			int d;
			cout<<"Enter data"<<endl;
			cin>>d;
			Node *n = new Node(d);
			Node *ptr;
			ptr = head;
			while(ptr->get_next_ptr() != NULL) {

				ptr = ptr->get_next_ptr();
			}
			ptr->set_next(n);
			n->set_prev(ptr);
			list_size++;

		}
	}

	void insert_interme() {

		int a;
		cout<<"Enter position"<<endl;
		cin>>a;

		if(a == 1)
			insert_beg();

		else if(a == list_size)
			insert_end();

		else if(a > list_size)
			cout<<"Enter a valid number because list isn't that long"<<endl;

		else {

			int d;
			cout<<"Enter data"<<endl;
			cin>>d;
			Node *n = new Node(d);
			Node *temp;
			Node *pre;
			temp = head;
			int i =1;
			while(i < a){
				pre = temp;
				temp = temp->get_next_ptr();
				i++;
			}

			pre->set_next(n);
			n->set_prev(pre);
			n->set_next(temp);
			temp->set_prev(n);
			list_size++;
		}
	}

	void delete_front() {
		if(head == NULL)
			cout<<"No element to delete"<<endl;

		else {
			Node *temp;
			temp = head;
			head = temp->get_next_ptr();
			head->set_prev(NULL);
			delete temp;
			list_size--;
		}

	}

	void delete_last(){
		if(head == NULL)
			cout<<"No element to delete"<<endl;
		else {
			Node *n, *pre;
			int i=1;
			n = head;
			while(i < list_size){
				pre = n;
				n = n->get_next_ptr();
				i++;
			}
			pre->set_next(NULL);
			delete n;
			list_size--;

		}
	}

	void delete_inter() {
		int a;
		cout<<"Enter the number of the node"<<endl;
		cin>>a;

		if(a == 1)
			delete_front();

		else if(a == list_size)
			delete_last();

		else if(a > list_size)
			cout<<"Enter a valid number because list isn't that long"<<endl;

		else {
			Node *temp, *n;
			int i = 1;
			temp = head;
			while(i < a){
			    n = temp;
				temp  = temp->get_next_ptr();
				i++;
			}
			n->set_next(temp->get_next_ptr());
            n = temp->get_next_ptr();
            n->set_prev(temp->get_prev_ptr());
			delete temp;
			list_size--;
		}
	}

	void no_element() {

		cout<<"Number of elements are "<<list_size<<endl;

	}

	void display_list(){
		Node *temp;
		temp = head;
		while(temp != NULL) {
			cout<<temp->get_data()<<endl;
			temp = temp->get_next_ptr();

		}

	}

	void disp_nth() {
		int a;
		cout<<"Enter the position of node"<<endl;
		cin>>a;
		int i = 1;

		Node *temp;
		temp = head;
		while (i < a) {

			temp = temp->get_next_ptr();
			i++;

		}
		cout<<"The requested data is "<<temp->get_data()<<endl;

	}

	void search_ele(int d){

		Node *tmp;
		tmp = head;
		int i = 1, flag = 0;

		while(tmp != NULL){
			if (tmp->get_data() == d){

				flag = 1;
				break;

			}
			else {
					tmp = tmp->get_next_ptr();
					i++;

			}
		}

		if(flag == 1)
			cout<<d<<" found at "<<i<<" position"<<endl;
		else
			cout<<"Not found"<<endl;
	}

	void print_rev(){

        Node *temp;
        temp = head;

        while(temp->get_next_ptr() != NULL){
            temp = temp->get_next_ptr();

        }
        while(temp != head){

            cout<<temp->get_data()<<endl;
            temp = temp->get_prev_ptr();
        }
            cout<<temp->get_data()<<endl;
	}


};

int main(){
link_list l;
int op,d;

cout<<"\n\t1.insert at beginning\n\t2.insert at last\n\t3.insert at intermediary\n\t4.remove at first"
	"\n\t5.remove at last\n\t6.remove a intermediary\n\t7.no. of elements\n\t8.search\n\t9.display\n\t10.display nth node\n\t11.Reverse list\n\t12.exit"<<endl;

cin>>op;

while(op<12){
	switch(op){

		case 1:l.insert_beg();
				break;
		case 2:l.insert_end();
				break;
		case 3:l.insert_interme();
				break;
		case 4:l.delete_front();
				break;
		case 5:l.delete_last();
				break;
		case 6:l.delete_inter();
				break;
		case 7:l.no_element();
				break;
		case 8:{cout<<"enter element to be searched"<<endl;cin>>d;l.search_ele(d);}
				break;
		case 9:l.display_list();
				break;
		case 10:l.disp_nth();
				break;
        case 11:l.print_rev();
                break;
		default: break;
		}

	cout<<"\n\t1.insert at beginning\n\t2.insert at last\n\t3.insert at intermediary\n\t4.remove at first"
	"\n\t5.remove at last\n\t6.remove a intermediary\n\t7.no. of elements\n\t8.search\n\t9.display\n\t10.display nth node\n\t11.Reverse list\n\t12.exit"<<endl;

    cin>>op;
	}

	return 0;


}
