/* Implementing queue using doubly linked list */
#include <iostream>
using namespace std;

class Node {
private:
	int data;
	Node *next, *prev;

public:
	Node():data(0),next(NULL), prev(NULL){}
	Node(int d):data(d),next(NULL), prev(NULL){}

	int get_data() {
		return data;
	}
	Node* get_next_ptr() {
		return next;
	}

	Node* get_prev_ptr(){
        return prev;
	}

	void set_node(int d) {
		data = d;

	}
	void set_next(Node *n) {
			next = n;
	}

	void set_prev(Node *n){
            prev = n;

	}
};

class doub_queue{

private:
	Node *head;
	int list_size;

public:

	doub_queue():head(NULL), list_size(0){}


	void insert_end() {

		int d;
        cout<<"Enter data"<<endl;
		cin>>d;
		Node *n = new Node(d);

		if(head == NULL) {
			head = n;
			list_size++;
		}

		else {

			Node *ptr;
			ptr = head;
			while(ptr->get_next_ptr() != NULL) {

				ptr = ptr->get_next_ptr();
			}
			ptr->set_next(n);
			n->set_prev(ptr);
			list_size++;

		}
	}

    	void delete_front() {
		if(head == NULL)
			cout<<"No element to delete"<<endl;

		else {
			Node *temp;
			temp = head;
			head = temp->get_next_ptr();
			head->set_prev(NULL);
			delete temp;
			list_size--;
		}

	}

	void no_element() {

		cout<<"Number of elements are "<<list_size<<endl;

	}

	void print_rev(){

        Node *temp;
        temp = head;

        while(temp->get_next_ptr() != NULL){

			cout<<temp->get_data()<<endl;
            temp = temp->get_next_ptr();

        }

		cout<<temp->get_data()<<endl;
	}



};
int main(){

doub_queue dq;
int op;

cout<<"\n\t1.Insert\n\t2.Delete\n\t3.Display\n\t4.No. of elements\n\t5.Exit"<<endl;
cin>>op;

while(op<5){

    switch(op){
        case 1:dq.insert_end();
            break;
        case 2:dq.delete_front();
            break;
        case 3:dq.print_rev();
            break;
        case 4:dq.no_element();
            break;
        default: break;

    }
    cout<<"\n\t1.Insert\n\t2.Delete\n\t3.Display\n\t4.No. of elements\n\t5.Exit"<<endl;
	cin>>op;

    }
    return 0;
}

