/* Stack using doubly linked list */

#include <iostream>
using namespace std;

class Node {
private:
	int data;
	Node *next, *prev;

public:
	Node():data(0),next(NULL), prev(NULL){}
	Node(int d):data(d),next(NULL), prev(NULL){}

	int get_data() {
		return data;
	}
	Node* get_next_ptr() {
		return next;
	}

	Node* get_prev_ptr(){
        return prev;
	}

	void set_node(int d) {
		data = d;

	}
	void set_next(Node *n) {
			next = n;
	}

	void set_prev(Node *n){
            prev = n;

	}
};
class stack {
private:
	Node *head;
	int list_size;

public:

	stack():head(NULL), list_size(0){}

    void push() {

        int d;
        cout<<"Enter data"<<endl;
		cin>>d;
		Node *n = new Node(d);
		if(head == NULL) {
			head = n;
			list_size++;
		}

		else {

			Node *ptr;
			ptr = head;
			while(ptr->get_next_ptr() != NULL) {

				ptr = ptr->get_next_ptr();
			}
			ptr->set_next(n);
			n->set_prev(ptr);
			list_size++;

		}
	}
    void pop(){
		if(head == NULL)
			cout<<"No element to delete"<<endl;
		else {
			Node *n, *pre;
			int i=1;
			n = head;
			while(i < list_size){
				pre = n;
				n = n->get_next_ptr();
				i++;
			}
			pre->set_next(NULL);
			delete n;
			list_size--;

		}
	}

	void no_element() {

		cout<<"Number of elements are "<<list_size<<endl;

	}

	void print_rev(){

        Node *temp;
        temp = head;

        while(temp->get_next_ptr() != NULL){
            temp = temp->get_next_ptr();

        }
        while(temp != head){

            cout<<temp->get_data()<<endl;
            temp = temp->get_prev_ptr();
        }
            cout<<temp->get_data()<<endl;
	}
};

int main(){

stack st;
int op;

cout<<"\n\t1.Push\n\t2.Pop\n\t3.Display\n\t4.No. of elements\n\t5.Exit"<<endl;
cin>>op;

while(op<5){

    switch(op){
        case 1:st.push();
            break;
        case 2:st.pop();
            break;
        case 3:st.print_rev();
            break;
        case 4:st.no_element();
            break;
        default: break;

    }
    cout<<"\n\t1.Push\n\t2.Pop\n\t3.Display\n\t4.No. of elements\n\t5.Exit"<<endl;
    cin>>op;

    }
    return 0;
}
