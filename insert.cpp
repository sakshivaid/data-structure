/* Sorting using insertion sort in descending order*/

#include <iostream>
using namespace std;

int main(){
int *arr, n ,i,j ,key;

cout<<"Enter the size of the array"<<endl;
cin>>n;

arr = new int[n];

cout<<"Enter the elements"<<endl;

for(j = 0 ; j<n; j++)
    cin>>arr[j];

for(j = 1; j<n; j++){
    key = arr[j];
    i = j-1;

    while((i>=0)&&(arr[i]<key)){
            arr[i+1]=arr[i];
            i=i-1;
        }

    arr[i+1]= key;

    }

for(j = 0 ; j<n; j++){
    cout<<arr[j]<<" ,";
    }
cout<<endl;
delete []arr;
return 0;
}
