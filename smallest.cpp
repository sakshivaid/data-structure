/* Finding out the smallest ASCII value of a char using linear search */
#include <iostream>
using namespace std;

int  main(){
char arr[10];
int check, index;

cout<<"enter 10 characters"<<endl;

for(int i =0 ; i<10; i++){
        cin>>arr[i];
    }

check = arr[0];

for(int i =1; i<10; i++){

    if(arr[i]<check){
            check=arr[i];
            index = i;
        }
    }

cout<<"Character with smallest ASCII value is "<<arr[index]<<" and its index is "<<index<<endl;

return 0;
}
