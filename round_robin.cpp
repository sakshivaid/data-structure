/* ROUND ROBIN */
#include <iostream>
using namespace std;

class round_robin {

int c_queue[10], front, rear, size, quantum;

public:

    round_robin():front(-1), rear(-1), size(10){}

    void input() {
        cout<<"Enter quantum "<<endl;
        cin>>quantum;

    }

    void insertion(char ch) {

        if(front == ((rear+1)%size))
            cout<<"Cannot handle more than this"<<endl;

        else if((front == -1)&&(rear == -1)) {
            front++;
            c_queue[++rear] = ch;
        }

        else {
            rear = (rear+1)%size;
            c_queue[rear] = ch;
        }

    }

    void deletion () {

        if((front == -1)&&(rear == - 1))
            cout<<"Done!"<<endl;

        else if((front == rear)&&(front != -1)){
            c_queue[front] == '#';
            front = rear = -1;
        }

        else {

            c_queue[front] == '#';
            front  = (front+1)%size;
        }


    }

    void output() {

        int i = front;

        while(i <= rear){
            cout<<c_queue[i]<<"\t"<<endl;
            i = ((i+1)%size);
        }

        cout<<"rear is "<<rear<<"\t front is "<<front<<endl;
    }

    void complete_process() {
        int i = front;

        while(i != (rear+1)%size) {
            c_queue[i] -= quantum;

            if(c_queue[i] <= 0) {

                for(int j = i; j < rear; j++) {

                  c_queue[j] = c_queue[j+1];
                }
                rear--;

            }


            i = ((i+1)%size);

        }


    }


};

int main() {
round_robin r;
int op, time;

r.input();
cout<<"Select an option"<<endl;
cout<<"1. Enter a processes' execution time. \n2. Present status of processor. \n3. Continue. \n4. Exit"<<endl;
cin>>op;

while(op != 4){

    switch(op){
        case 1: {cout<<"Enter"<<endl;cin>>time; r.insertion(time);}
            break;
        case 2: r.output();
            break;
        case 3: r.complete_process();
            break;
        default : break;
        }

    cout<<"Select an option"<<endl;
    cout<<"1. Enter a processes' execution time. \n2. Present status of processor. \n3. Continue. \n4. Exit"<<endl;
    cin>>op;

    }
r.output();
return 0;
}

