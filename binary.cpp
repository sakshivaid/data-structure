/* Searching a key in an unsorted array using binary search */

#include <iostream>
using namespace std;

int main(){
int *arr;
int index, high, low, mid, n, key,temp, flag=0;

cout<<"Enter the size of the array"<<endl;
cin>>n;

arr = new int[n];
cout<<"Enter the elements of the array"<<endl;

for(int i=0; i<n; i++){
        cin>>arr[i];
    }

for(int i=0; i<n; i++){

    for(int j=n-1; j>i;j--){

        if(arr[j]<arr[j-1]){
            temp = arr[j];
            arr[j]=arr[j-1];
            arr[j-1]=temp;
            }
        }
    }
for(int i=0; i<n; i++){
    cout<<arr[i]<<"\t";
    }
    cout<<endl;

cout<<"Enter the key to be searched"<<endl;
cin>>key;

low =0;
high = n-1;
mid = (low+high)/2;

while(low <= high){

    if(key==arr[mid]){
        index = mid;
        flag = 1;
        }

   else if(key<arr[mid]){
        high = mid;
        mid= (low+high)/2;
        }


    else{
        low =mid;
        mid = (low+high)/2;
        }
    }
if(flag ==1)
cout<<"The key is found in position "<<index<<endl;
else
cout<<"Key not found"<<endl;
}
