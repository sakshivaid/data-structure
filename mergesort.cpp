/* Implementing Merge sort */
#include <iostream>
using namespace std;

class mergesort {
int *arr, *temp;

public:
int n;		//size of the array

	void input() {
		cout<<"Enter the size"<<endl;
		cin>>n;
		arr = new int[n];		// allocating memory dynamically
		temp = new int[n];

		cout<<"Input the values"<<endl;
		for(int i = 0; i<n; i++)
			cin>>arr[i];

		}

	void split_it(int low, int high) {
		int mid;

		if(low < high) {
			mid = (low+high)/2;
			split_it(low, mid);
			split_it(mid+1, high);
			merge_it(low, mid, high);
		}
	}

	void merge_it(int low, int mid, int high) {

		int store_mid = mid, index = low;
		int store_low = low;

		while((low <= store_mid)&&(mid+1 <= high)) {

			if(arr[low] <= arr[mid+1]) {
				temp[index] = arr[low];
				low++;
			}

			else {
				temp[index] = arr[mid+1];
				mid++;
			}
			index++;
		}

		if(low > store_mid) {

			for(int i = mid+1; i <= high; i++) {
				temp[index]=arr[i];

				index++;
			}


		}
		else {

			for(int i = low; i <= store_mid; i++) {

				temp[index] = arr[i];
				index++;
			}
		}


    for(int i = store_low; i <= high; i++) {
		arr[i] = temp[i];

		}
	}

	void output(){

		for(int i = 0; i < n; i++)
			cout<<arr[i]<<"\t";
		cout<<endl;

	}


};

int main() {
mergesort merg;
merg.input();
merg.split_it(0, merg.n-1);
merg.output();

}

