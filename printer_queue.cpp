/* Implementing simple queue , printing requests */
#include <iostream>
using namespace std;

class printer_queue {

int queue[15], front, rear, size;

public:
    printer_queue():front(-1), rear(-1), size(15){}

    void enqueue() {

        int comp_no;
        cout<<"Enter system number"<<endl;
        cin>>comp_no;

        if(rear == size-1)
            cout<<"Queue is full, OVERFLOW!"<<endl;


        else if((front ==-1)&&(rear == -1)) {
            front++;
            queue[++rear]=comp_no;
        }

        else {

            queue[++rear] = comp_no;
        }

    }

    void dequeue() {

        if((front ==-1)&&(rear == -1))
            cout<<"Empty queue, UNDERFLOW!"<<endl;

        else if((front == rear)&&(front != -1)){

            queue[front] = 0;
            front = rear = -1;

        }

        else
            queue[front++] = 0;

    }

    void output() {

        int i = front;
        while(i <= rear){
            cout<<queue[i]<<"\t"<<endl;
            i++;
        }
        cout<<"rear is "<<rear<<"\t front is "<<front<<endl;
    }


};

int main(){
printer_queue p;
int op;
cout<<"Select an option"<<endl;
cout<<"1. Using printer \n2. Job done \n3. Output\n4. Exit"<<endl;
cin>>op;
while(op != 4){

    switch(op){
        case 1: p.enqueue();
            break;
        case 2: p.dequeue();
            break;
        case 3 : p.output();
            break;
        default: break;
        }
    cout<<"Select an option"<<endl;
    cout<<"1. Using printer \n2. Job done \n3. Output\n4. Exit"<<endl;
    cin>>op;
    }
p.output();
return 0;
}
