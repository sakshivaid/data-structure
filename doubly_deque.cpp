/* Implementing deque using doubly linked list */

#include <iostream>
using namespace std;

class Node {
private:
	int data;
	Node *next, *prev;

public:
	Node():data(0),next(NULL), prev(NULL){}
	Node(int d):data(d),next(NULL), prev(NULL){}

	int get_data() {
		return data;
	}
	Node* get_next_ptr() {
		return next;
	}

	Node* get_prev_ptr(){
        return prev;
	}

	void set_node(int d) {
		data = d;

	}
	void set_next(Node *n) {
			next = n;
	}

	void set_prev(Node *n){
            prev = n;

	}
};

class doub_deque {
private:
	Node *head;
	int list_size;

public:

	doub_deque():head(NULL), list_size(0){}

	void insert_beg() {
		int d;
		cout<<"Enter data"<<endl;
		cin>>d;
		Node *n = new Node(d);

		if(head == NULL){
			head = n;
			list_size++;
		}

		else {

			n->set_next(head);
			head->set_prev(n);
			head = n;
			list_size++;
		}
	}

	void insert_end() {


		if(head == NULL) {
			insert_beg();
		}

		else {

			int d;
			cout<<"Enter data"<<endl;
			cin>>d;
			Node *n = new Node(d);
			Node *ptr;
			ptr = head;
			while(ptr->get_next_ptr() != NULL) {

				ptr = ptr->get_next_ptr();
			}
			ptr->set_next(n);
			n->set_prev(ptr);
			list_size++;

		}
	}


	void delete_front() {
		if(head == NULL)
			cout<<"No element to delete"<<endl;

		else {
			Node *temp;
			temp = head;
			head = temp->get_next_ptr();
			head->set_prev(NULL);
			delete temp;
			list_size--;
		}

	}

	void delete_last(){
		if(head == NULL)
			cout<<"No element to delete"<<endl;
		else {
			Node *n, *pre;
			int i=1;
			n = head;
			while(i < list_size){
				pre = n;
				n = n->get_next_ptr();
				i++;
			}
			pre->set_next(NULL);
			delete n;
			list_size--;

		}
	}


	void no_element() {

		cout<<"Number of elements are "<<list_size<<endl;

	}

	void display_list(){
		Node *temp;
		temp = head;
		while(temp != NULL) {
			cout<<temp->get_data()<<endl;
			temp = temp->get_next_ptr();

		}

	}



};
int main(){

doub_deque dd;
int op;

cout<<"\n\t1.Insert at fornt\n\t2.Delete at front\n\t3.Insert at rear\n\t4.Delete at rear\n\t5.Dispaly\n\t6.No. of elements\n\t7.Exit"<<endl;
cin>>op;

while(op<7){

    switch(op){
        case 1:dd.insert_beg();
            break;
        case 2:dd.delete_front();
            break;
        case 3:dd.insert_end();
            break;
        case 4:dd.delete_last();
            break;
		case 5:dd.display_list();
			break;
		case 6:dd.no_element();
			break;
        default: break;

    }
    cout<<"\n\t1.Insert at fornt\n\t2.Delete at front\n\t3.Insert at rear\n\t4.Delete at rear\n\t5.Dispaly\n\t6.No. of elements\n\t7.Exit"<<endl;
	cin>>op;


    }
    return 0;
}
