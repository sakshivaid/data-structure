/* Sorting an integer array in decending order using bubble sort */

#include <iostream>
using namespace std;

int main(){

int *arr;
int n, temp;//temp is the temporary variable and n is to get the size of the array
cout<<"Enter the size of the array"<<endl;
cin>>n;

arr= new int[n];

cout<<"Enter the elements of the array"<<endl;

for(int i=0; i<n; i++)
    cin>>arr[i];

for(int i=0; i<n; i++){

    for(int j=n-1; j>i;j--){

        if(arr[j]>arr[j-1]){
            temp = arr[j];
            arr[j]=arr[j-1];
            arr[j-1]=temp;
            }
        }
    }

    for(int i=0 ; i<n ; i++)
        cout<<arr[i]<<" ,";
    cout<<endl;
    return 0;
}
