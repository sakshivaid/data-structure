/*Implementing a deque using singly linked list (insertion restricted)*/

#include <iostream>
using namespace std;

class Node {
private:
	int data;
	Node *next;

public:
	Node():data(0),next(NULL){}
	Node(int d):data(d),next(NULL){}

	int get_data() {
		return data;
	}
	Node* get_next_ptr() {
		return next;
	}
	void set_node(int d) {
		data = d;

	}
	void set_next(Node *n) {
			next = n;
	}
};

class link_deque {
private:
	Node *head;
	int list_size;

public:

	link_deque():head(NULL), list_size(0){}

	void enqueue_rear() {


		if(head == NULL) {
			int d;
			cout<<"Enter data"<<endl;
			cin>>d;
			Node *n = new Node(d);
			n->set_next(NULL);
			head = n;
			list_size++;
		}

		else {

			int d;
			cout<<"Enter data"<<endl;
			cin>>d;
			Node *n = new Node(d);
			Node *ptr;
			ptr = head;
			while(ptr->get_next_ptr() != NULL) {

				ptr = ptr->get_next_ptr();
			}
			ptr->set_next(n);
			n->set_next(NULL);
			list_size++;

		}
	}
	void dequeue_front() {
		if(head == NULL)
			cout<<"No element to delete"<<endl;

		else {
			Node *temp;
			temp = head;
			head = temp->get_next_ptr();
			delete temp;
			list_size--;
		}

	}

	void dequeue_rear(){
		if(head == NULL)
			cout<<"No element to delete"<<endl;
		else {
			Node *n, *pre;
			int i=1;
			n = head;
			while(i < list_size){
				pre = n;
				n = n->get_next_ptr();
				i++;
			}
			pre->set_next(NULL);
			delete n;
			list_size--;

		}
	}

	void no_element() {

		cout<<"Number of elements are "<<list_size<<endl;

	}

	void display_list(){
		Node *temp;
		temp = head;
		while(temp != NULL) {
			cout<<temp->get_data()<<endl;
			temp = temp->get_next_ptr();

		}

	}

};
int main(){

link_deque dq;
int op;

cout<<"\n\t1.Enqueue from rear\n\t2.Dequeue from front\n\t3.Dequeue from rear\n\t4.No. of elements\n\t5.Display\n\t6.Exit"<<endl;
cin>>op;

while(op<6){
	switch(op){
		case 1:dq.enqueue_rear();
			break;
		case 2:dq.dequeue_front();
			break;
		case 3:dq.dequeue_rear();
			break;
		case 4:dq.no_element();
			break;
		case 5:dq.display_list();
			break;
		default:
			break;
		}
	cout<<"\n\t1.Enqueue from rear\n\t2.Dequeue from front\n\t3.Dequeue from rear\n\t4.No. of elements\n\t5.Display\n\t6.Exit"<<endl;
	cin>>op;
	}
	return 0;
}

