/* Implement MinHeap and sort a list of integers */
#include <iostream>
using namespace std;

class min_heap{

int *heap, h_size;

public:

	min_heap(){
		cout<<"Enter the number of elements in the heap"<<endl;
		cin>>h_size;
		h_size++;

		heap = new int[h_size];

	}

	void insert_ele(){

		int j;
		for(int i = 1;i < h_size; i++){

			cout<<"Enter the data"<<endl;
			cin>>heap[i];
			j = i;

			while(j > 1){

				if(heap[j] < heap[(j)/2]){

					int temp = heap[j];
					heap[j] = heap[(j)/2];
					heap[(j)/2] = temp;

					j = (j)/2;
				}

				else
					break;
			}
		}
	}

	void print_heap(){

		for(int i = 1;i < h_size; i++)
			cout<<heap[i]<<", ";

		cout<<endl;
	}

	void sorting(){

		while(h_size > 1){

			cout<<heap[1]<<", ";
			heap[1] = heap[h_size-1];
			h_size--;
			min_heapify(1);
		}
		cout<<endl;
	}



	void min_heapify(int i){

		int left, right, smallest;
		left = 2*i;
		right = 2*i +1;
		smallest = i;

		if((left <= h_size)&&(heap[left]<heap[smallest])){
			smallest = left;
		}
		if((right <= h_size)&&(heap[right] < heap[smallest])){
			smallest = right;
		}
		if(smallest != i){
			swap(heap[i], heap[smallest]);
			min_heapify(smallest);
		}

	}

};

int main(){

min_heap m;
m.insert_ele();
m.print_heap();
m.sorting();
return 0;

}
