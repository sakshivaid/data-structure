#include <iostream>
using namespace std;

class Node {
private:
	int data;
	Node *next;

public:
	Node():data(0),next(NULL){}
	Node(int d):data(d),next(NULL){}

	int get_data() {
		return data;
	}
	Node* get_next_ptr() {
		return next;
	}
	void set_node(int d) {
		data = d;

	}
	void set_next(Node *n) {
			next = n;
	}
};
class link_queue {
private:
	Node *head;
	int list_size;

public:

	link_queue():head(NULL), list_size(0){}
	void enqueue() {


		if(head == NULL) {
			int d;
			cout<<"Enter data"<<endl;
			cin>>d;
			Node *n = new Node(d);

			n->set_next(NULL);
			head = n;
			list_size++;
		}

		else {

			int d;
			cout<<"Enter data"<<endl;
			cin>>d;
			Node *n = new Node(d);
			Node *ptr;
			ptr = head;
			while(ptr->get_next_ptr() != NULL) {

				ptr = ptr->get_next_ptr();
			}
			ptr->set_next(n);
			n->set_next(NULL);
			list_size++;

		}
	}

	void dequeue() {
		if(head == NULL)
			cout<<"No element to delete"<<endl;

		else {
			Node *temp;
			temp = head;
			head = temp->get_next_ptr();
			delete temp;
			list_size--;
		}

	}

	void no_element() {

		cout<<"Number of elements are "<<list_size<<endl;

	}
	void display_list(){
		Node *temp;
		temp = head;
		while(temp != NULL) {
			cout<<temp->get_data()<<endl;
			temp = temp->get_next_ptr();

		}

	}
};

int main(){

link_queue q;
int op;

cout<<"\n\t1.Enqueue\n\t2.Dequeue\n\t3.No. of elements\n\t4.Display\n\t5.Exit"<<endl;
cin>>op;

while(op<5){
	switch(op){
		case 1:q.enqueue();
			break;
		case 2:q.dequeue();
			break;
		case 3:q.no_element();
			break;
		case 4:q.display_list();
			break;
		default:
			break;
		}
	cout<<"\n\t1.Enqueue\n\t2.Dequeue\n\t3.No. of elements\n\t4.Display\n\t5.Exit"<<endl;
	cin>>op;
	}
	return 0;
}
