/* Implementing stack using singly linked list */

#include <iostream>
using namespace std;

class Node {
private:
	int data;
	Node *next;

public:
	Node():data(0),next(NULL){}
	Node(int d):data(d),next(NULL){}

	int get_data() {
		return data;
	}
	Node* get_next_ptr() {
		return next;
	}
	void set_node(int d) {
		data = d;

	}
	void set_next(Node *n) {
			next = n;
	}
};

class link_stack {

private:
	Node *head;
	int list_size;

public:

	link_stack():head(NULL), list_size(0){}


	void push() {

		if(head == NULL) {
			int d;
			cout<<"Enter data"<<endl;
			cin>>d;
			Node *n = new Node(d);
			n->set_next(NULL);
			head = n;
			list_size++;
		}

		else {

			int d;
			cout<<"Enter data"<<endl;
			cin>>d;
			Node *n = new Node(d);
			Node *ptr;
			ptr = head;
			while(ptr->get_next_ptr() != NULL) {

				ptr = ptr->get_next_ptr();
			}
			ptr->set_next(n);
			n->set_next(NULL);
			list_size++;

		}
	}

	void pop(){
		if(head == NULL)
			cout<<"No element to delete"<<endl;
		else {
			Node *n, *pre;
			int i=1;
			n = head;
			while(i < list_size){
				pre = n;
				n = n->get_next_ptr();
				i++;
			}
			pre->set_next(NULL);
			delete n;
			list_size--;

		}
	}

	void no_element() {

		cout<<"Number of elements are "<<list_size<<endl;

	}
	void display_list(){
		Node *temp;
		temp = head;
		while(temp != NULL) {
			cout<<temp->get_data()<<endl;
			temp = temp->get_next_ptr();

		}

	}
};

int main(){

link_stack ls;
int op;

cout<<"\n\t1.Push\n\t2.Pop\n\t3.No. of elements\n\t4.Display\n\t5.Exit"<<endl;
cin>>op;

while(op<5){
	switch(op){
		case 1:ls.push();
			break;
		case 2:ls.pop();
			break;
		case 3:ls.no_element();
			break;
		case 4:ls.display_list();
			break;
		default:
			break;
		}
	cout<<"\n\t1.Push\n\t2.Pop\n\t3.No. of elements\n\t4.Display\n\t5.Exit"<<endl;
	cin>>op;
	}
	return 0;
}