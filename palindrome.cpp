/* Implementing palindrom checker using deque */
#include <iostream>
#include <string>
using namespace std;

class palindrome {
int front, rear, length, flag;
string deque;

public:
	palindrome ():front(-1), rear(-1), flag(0){}

	void input() {

		cout<<"Enter a word"<<endl;
		cin>>deque;

		length = deque.size();
		front = 0;
		rear = length-1;

	}

	char deque_front() {

		if(front == -1) {
			//cout<<"Cannot empty the queue, already empty!"<<endl;
		}

		else {
			if((front == length-1)||(front == rear)) {
				char ch = deque[front];

				front = -1;
				rear = -1;
				return ch;

			}

			else {
				return deque[front++];

			}

		}
	}

	char deque_rear() {

		if(rear== -1) {
			//cout<<"Cannot empty the queue, already empty!"<<endl;
		}

		else {

			if ((front == length-1)||(front == rear)) {

				char ch = deque[rear];
				front = -1;
				rear = -1;

				return ch;
			}

			else {
				return deque[rear--];
			}
		}

	}

	void check() {
		while(rear != -1) {

			if(deque_front() != deque_rear()){
				flag = 1;
			}

			if(front == rear) {
				rear = -1;
				front = -1;
			}

		}
	}

	void output() {

		if(flag == 0)
			cout<<"It is a Palindrome"<<endl;
		else
			cout<<"It is not a Palindrome"<<endl;
	}
};


int main(){

palindrome p;
p.input();
p.check();
p.output();

return 0;
}

