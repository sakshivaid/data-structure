/* Implementing ticket counter queue */
#include<iostream>
using namespace std;

class ticket_queue {
string queue[10];
int front, rear, size;

public:
    ticket_queue():front(-1), rear(-1), size(10){}

    void enqueue() {

        string name;
        cout<<"Enter the name"<<endl;
        cin>>name;

        if(rear == size-1)
            cout<<"Queue is full, OVERFLOW!"<<endl;


        else if((front ==-1)&&(rear == -1)) {
            front++;
            queue[++rear]=name;
        }

        else {

            queue[++rear] = name;
        }

    }

    void dequeue() {

        if((front ==-1)&&(rear == -1))
            cout<<"Empty queue, UNDERFLOW!"<<endl;

        else if((front == rear)&&(front != -1)){

            queue[front] = 'NULL';
            front = rear = -1;

        }

        else
            queue[front++] = 'NULL';

    }

    void output() {

        int i = front;
        while(i <= rear){
            cout<<queue[i]<<"\t"<<endl;
            i++;
        }
        cout<<"rear is "<<rear<<"\t front is "<<front<<endl;
    }


};

int main(){
ticket_queue t;
int op;
cout<<"Select an option"<<endl;
cout<<"1. Need a ticket \n2. Got a ticket \n3. Present queue status\n4. Exit"<<endl;
cin>>op;
while(op != 4){

    switch(op){
        case 1: t.enqueue();
            break;
        case 2: t.dequeue();
            break;
        case 3 : t.output();
            break;
        default: break;
        }
    cout<<"Select an option"<<endl;
    cout<<"1. Need a ticket \n2. Got a ticket \n3. Present queue status\n4. Exit"<<endl;
    cin>>op;
    }
t.output();
return 0;

}
